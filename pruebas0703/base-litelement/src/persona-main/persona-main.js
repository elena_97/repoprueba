 import {LitElement, html} from 'lit';
 import '../persona-ficha-listado/persona-ficha-listado.js';

 
 class PersonaMain extends LitElement{
    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Dobby",
                yearsInCompany: 6,
                photo: {
                    src: "./img/dobby.jpg",
                    alt: "Dobby, el elfo"
                },
                profile: "frase ejemplo dobby"
            },{
                name: "Lisa",
                yearsInCompany: 8,
                photo: {
                    src: "./img/lisa.jpg",
                    alt:"Lisa Simpson"
                },
                profile: "frase ejemplo lisa"
            },{
                name: "Miercoles",
                yearsInCompany: 16,
                photo: {
                    src: "./img/miercoles.jpg",
                    alt:"Miercoles Addams"
                },
                profile: "frase ejemplo  miercoles"
            },{
                name: "Leia",
                yearsInCompany: 55,
                photo: {
                    src: "./img/leia.jpg",
                    alt:"Princesa Leia"
                },
                profile: "frase ejemplo  leia"
            },{
                name: "Peter",
                yearsInCompany: 12,
                photo: {
                    src: "./img/peterpan.jpg",
                    alt:"Peter Pan"
                },
                profile: "frase ejemplo  peter"
            }
            
        ]

    }
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas - Main</h2>
            <br />
            <div class="row" id="peopleList">
                <main class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html` <persona-ficha-listado
                                            fname="${person.name}"
                                            yearsInCompany="${person.yearsInCompany}"
                                            profile="${person.profile}"
                                            .photo="${person.photo}"
                                            @delete-person="${this.deletePerson}"
                                        ></persona-ficha-listado>`                
                    )}
                </main>
            </div>
        `
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log(e.detail);
        console.log("Se va borrar la persona " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }


 }

 customElements.define("persona-main", PersonaMain);