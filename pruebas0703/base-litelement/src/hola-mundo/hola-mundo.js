 import {LitElement, html} from 'lit';
 
 class HolaMundo extends LitElement{

    render(){
        return html`
            <div>HolaMundo!</div>
        `
    }
 }

 customElements.define("hola-mundo", HolaMundo);