 import { LitElement, html} from 'lit';
 
 class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsinCompany: {type: Number},
            personInfo: {type: String}
        }
    }

    constructor() {
        super();
        this.name = "elena";
        this.yearsinCompany = 12;
        this.updatePersonInfo(); 

    }

    updated(changedproperties) {
        changedproperties.forEach(
            (oldValue, propName) => {
                console.log("Propiedad " + propName
                    + " ha cambiado de valor, anterior era " + oldValue);
            }  
            
        );

        if(changedproperties.has("name")) {
            console.log("Propiedad name cambia de valor, anterior era "
            + changedproperties.get("name") + " nuevo es " + this.name);
        }

        if(changedproperties.has("yearsinCompany")) {
            console.log("Propiedad yearsinCompany cambia de valor, anterior era "
            + changedproperties.get("yearsinCompany") + " nuevo es " + this.yearsinCompany);

            this.updatePersonInfo();
        }

    }

    render() {
        return html`
            <div>
                <label>Nombre Completo</label>
                <!-- <input type="text" id="fname" value="${this.name}" @change="${this.updateName}"></input> -->
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"/>
                <br />
                <label>Años en la empresa</label>
                <input type="text" value="${this.yearsinCompany}" @input="${this.updateYearsInCompany}"/>
                <br />
                <input type="text" value="${this.personInfo}" disabled />
                <br />
            </div>
            `
    }

    updateName(e) {
        console.log("updateName");
        //console.log(e);
        this.name = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        //console.log(e);
        this.yearsinCompany = e.target.value;

    }

    updatePersonInfo(){
        console.log("updatepersonInfo");
        console.log("propiedad yearsinCompany value " + this.yearsinCompany);
        
        if(this.yearsinCompany >= 7) {
            this.personInfo = "lead";
        } else if(this.yearsinCompany >= 5){
            this.personInfo = "senior";
        }else if(this.yearsinCompany >= 3){
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }
 }

 customElements.define("ficha-persona", FichaPersona);